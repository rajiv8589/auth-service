package com.garbage.authservice.authservice.contoller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/welcome")
    public ResponseEntity<?> welcome(){
        return ResponseEntity.ok("Bingo!");
    }
}
